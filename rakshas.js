/*
     ____          _    _   ____    _    _            ____
    /  __|   __   | | / /  / ___|  | |  | |     __   / ___|
    | |     /  |  | |/ /   \_|__   | |__| |    /  |  \_|__
    | |    / / |  | |\ \   ___| \  |  __  |   / / |  ___| \
    |_|   /_/|_|  |_| \_\  |___ /  |_|  |_|  /_/|_|  |___ /


      /|  |\
     / |__| \
    |        |
    |  O  O  |
    |        |
    |/\/\/\/\|

*/

var rakshas = (function () {
    function Game(div, id, w, h) {
        /* the canvas is created with the id passed into the function,
        the context is used for 2D drawing, width and height are passed
        into the function and represent the canvas width and height */
        var canvas = document.createElement('canvas');
        var context = canvas.getContext("2d");
        var width = w;
        var height = h;

        /* scenes is an array that holds the list of all the scenes created 
        of the type Scene() images is an object that holds the list of 
        all images loaded, these images will used to create static and animated
        sprites. */
        var scenes = [];
        var images = {};

        /* pixelated rendering is used to remove the bluriness of the sprites
        shapes drawn on the screen, it disables the anti-alias. Good for making
        games with retro pixel art style */
        var pixelatedRendering = false;

        /* append the created canvas element to the body to make it visible
        set the canvas id to the id passed in the function, also set the height
        and width. Make the border visible 1px solid black line.
        
        TODO: Give an option to disable the border */
        document.getElementById(div).appendChild(canvas);
        canvas.id = id;
        canvas.width = w;
        canvas.height = h;
        canvas.tabIndex = 1;
        canvas.style.border = "1px solid";
        canvas.style.margin = "0 auto";
        canvas.style.display = "block";

        //return the canvas object
        this.getCanvas = function () {
            return canvas;
        }

        //returns the context object
        this.getContext = function () {
            return context;
        }

        //returns the dimensions in an object
        this.getDimensions = function () {
            return {
                width: width,
                height: height
            }
        }

        //returns the scenes list object, this will not be used in most of the common cases
        this.getScenes = function () {
            return scenes;
        }

        //returns the images list object
        this.getImages = function () {
            return images
        }

        //returns if the pixelated rendering is true or not
        //returns boolean
        this.isPixelatedRendering = function () {
            return pixelatedRendering;
        }

        //set the dimensions of the canvas after the initial creation
        this.setDimensions = function (w, h) {
            width = w;
            height = h;
        }

        //set the pixelatedRendering property to true/false
        this.setPixelatedRendering = function (bool) {
            pixelatedRendering = bool;
            if (pixelatedRendering) {
                canvas.style.imageRendering = "pixelated";
            } else {
                canvas.style.imageRendering = "optimized";
            }
        }

        /* 
        preloads all the images
        count is the number of images that have been loaded,
        length is the total number of images to be loaded
        
        TODO: Maybe stop the explicit call of the start() function,
        pass it as a callback to preload function so the game only starts
        after all the assets have been loaded
        */
        var preload = function (imagelist, callback) {
            var count = 0;
            var length = 0;
            var isEmpty = function(obj) {
                for(var i in obj) {
                    if(obj.hasOwnProperty(i))
                        return false;
                }
                return true;
            }
                        
            if(isEmpty(imagelist))
                callback();
            
            /*
            this loop calculates the total number of images to be loaded
            the first if condition checks if the current object of the imageList
            passed is an string(single image) or an array(multiple images).
            If the obect is a string, increment length by 1, or else if the objet
            is an Array, increment the length by the length of the array object
            */

            for (var i in imagelist) {
                if (typeof (imagelist[i]) === "string") {
                    length++;
                }
                if (imagelist[i].constructor === Array) {
                    length += imagelist[i].length;
                }
            }

            /*
            this loop is used to actually load all the images,
            we again iterate over the imageList passed.
            the first if condition checks if the current object is a string
            (single image), then we append the image to the images property.
            After the image is loaded, the count is incremented by one and
            checked if count is equal to length or not. If yes, then return 
            or call the callback function.
            
            The second if condition checks if the current object is an Array,
            then we run append an array to the images property and run another
            loop for that object and add all the loaded images to the array we
            just appended.
            */
            for (var i in imagelist) {
                if (typeof (imagelist[i]) === "string") {
                    images[i] = new Image();
                    images[i].src = imagelist[i];
                    images[i].onload = function () {
                        count++;
                        if (count >= length) {
                            if (callback === undefined) {
                                return;
                            } else {
                                callback();
                            }
                        }
                    }
                }
                if (imagelist[i].constructor === Array) {
                    images[i] = [];
                    for (var j in imagelist[i]) {
                        images[i][j] = new Image();
                        images[i][j].src = imagelist[i][j];
                        images[i][j].onload = function () {
                            count++;
                            if (count >= length) {
                                if (callback === undefined) {
                                    return;
                                } else {
                                    callback();
                                }
                            }
                        }
                    }
                }
            }
        }

        /*
        thie meat of the game logic
        this function is called every frame, it takes the update of function of
        currently active scene and executes them, along with drawing them on the
        context and some other shenanigans
        This is not publicly accesible
        */
        var update = function () {
            for (var i = 0; i < scenes.length; i++) {
                if (scenes[i].isSceneActive() == 2) {
                    scenes[i].init();
                    scenes[i].setSceneActive(1);
                }
                if (scenes[i].isSceneActive() == 1) {
                    scenes[i].update();
                    scenes[i].draw();
                }
            }
            requestAnimationFrame(update);
        }

        /*
        publicly accessible function used to start the game, the list of images 
        to be loaded is passed as a parameter along with a callback function which
        will be executed after the loading has been finished. This callback is
        usually the update function of the game, which is called as soon as all 
        the required images are loaded. The update and preload methods have to 
        be coupled this way because Javascript is asyncronous, so if they are 
        called seprately, the update function will start executing without 
        waiting for the preloading of the images to finish
        */
        this.load = function (resources, gameFunction) {
            preload(resources, gameFunction);
        }
        this.start = function (imagelist) {
            console.log("game started");
            update();
        }

        /*
        this function is used to set the current scene, i.e the function whose update
        and init function will be execute by the main update loop
        */
        this.setActiveScene = function (scene) {
            for (var i = 0; i < scenes.length; i++) {
                if (scenes[i].isSceneActive()) {
                    scenes[i].setSceneActive(0);
                    scenes[i].resetScene();
                }
            }
            scene.setSceneActive(2);
        }
    }

    function Scene(game, scene) {
        // keep a local instance of game for reference
        var game = game;
        //is the scene active
        var isActive = 0;

        /* 
        check if the scene object passed has its own localData property
        or not, if yes then set it to the localData property of the new 
        scene, or else make a empty new one
        */
        if (scene.hasOwnProperty('localData')) {
            this.localData = scene.localData;
            var originalLocalData = scene.localData;
        } else {
            this.localData = {};
            var originalLocalData = {};
        }

        /*
        check if an init function was passed or not, if no then
        create an empty one with warning. This init function is 
        executed once when the scene is loaded, mainly to set
        initial settings
        */
        if (typeof (scene.init) === "function") {
            this.init = scene.init;
        } else {
            this.init = function () {
                console.log("No init function defined for the scene");
            }
        }

        /*
        same as above, but the filter function is executed every
        frame. Can be used for adding effects. Currently almost 
        useless since it grinds the game to a halt. Manipulating
        the whole canvas(every pixel) every single frame without webgl is not
        a good idea. Avoid this function as plague. Also this 
        function is optional anyway. ^__^
        */
        if (typeof (scene.filter) === "function") {
            this.filter = scene.filter;
        } else {
            this.filter = function () {}
        }

        if (typeof (scene.update) === "function") {
            this.update = scene.update;
        } else {
            this.update = function () {}
        }

        //list of all entities used by this scene
        this.entities = {};
        this.drawStack = [];

        /*
        in this loop, we iterate over the entities array of the scene object passed
        to this function and create new objects for each entity passed
        All of these entities are stored in the entities object. They can be
        accessed in the init and update function of each scene
        */
        for (var i = 0; i < scene.entities.length; i++) {
            if (scene.entities[i].type === "StaticSprite") 
                this.entities[scene.entities[i].name] = new StaticSprite(scene.entities[i]);
            if (scene.entities[i].type === "AnimatedSprite") 
                this.entities[scene.entities[i].name] = new AnimatedSprite(scene.entities[i]);
        }

        /*
        here we just add all the entities we have to the drawStack
        */
        for (var i in this.entities) {
            this.drawStack.push(this.entities[i]);
        }

        /*
        this function goes through all the entities in the drawStack and calls
        their respective draw function. We could also just simply iterate through
        the entities property and call the draw function, but having a separate
        draw stack allows us to also draw things which are not entities, for eg.
        primitive shapes, text etc. Primitive shapes and text dont have instances
        like normal sprites
        */
        this.draw = function () {

            for (var i = 0; i < this.drawStack.length; i++) {
                this.drawStack[i].draw(game.getContext());
            }

            /*
            the draw stack is emptied every frame and the entitiesare pushed back
            into the stack
            
            */
            this.drawStack = [];
            for (var i in this.entities) {
                this.drawStack.push(this.entities[i]);
            }
        }

        // returns if the scene is active or not
        this.isSceneActive = function () {
            return isActive;
        }

        /* 
        sets the value for the isActive variable, used internally by the library
        user should use the Game.setActiveScene() method
        */
        this.setSceneActive = function (val) {
            isActive = val;
        }

        this.getGame = function () {
            return game;
        }


        //push the scene into the scene list of the game object
        game.getScenes().push(this);
    }

    function AnimatedSprite(sprite) {
        var type = sprite.type;
        var originalWidth = sprite.width;
        var originalHeight = sprite.height;
        var drawable = true;
        /*
        instance[i] = {
            x,
            y,
            width,
            height,
            angle,
            alpha,
            currentAnimation,
            currentFrame,
            animationTimer
        }
        */
        var animationList = sprite.animationList;
        var instance = [];
        if (sprite.pivot !== undefined) {
            var pivot = {
                x: sprite.pivot.x,
                y: sprite.pivot.y
            }
        } else {
            var pivot = {
                x: 0,
                y: 0
            }
        }
        var count = 0;

        this.draw = function (context) {
            for (var i = 0; i < count; i++) {
                context.save();
                context.translate(instance[i].x, instance[i].y);
                context.globalAlpha = instance[i].alpha;
                
                context.drawImage(animationList[instance[i].currentAnimation].frames[instance[i].currentFrame], parseInt(-instance[i].width * pivot.x), parseInt(-instance[i].height * pivot.y), instance[i].width, instance[i].height);
                
                instance[i].animationTimer += animationList[instance[i].currentAnimation].speed;
                if(instance[i].animationTimer >= 1) {
                    if (animationList[instance[i].currentAnimation].type === 'repeat') {
                        if (instance[i].currentFrame == animationList[instance[i].currentAnimation].frames.length - 1) {
                            instance[i].currentFrame = 0;
                        } else {
                            instance[i].currentFrame += 1;
                        }
                    }
                    if (animationList[instance[i].currentAnimation].type === 'once') {
                        if (instance[i].currentFrame == animationList[instance[i].currentAnimation].frames.length - 1) {
                            //this.removeInstance(i);
                            //do nothing, let the user decide if they want to remove the sprite
                            //after it has finished playing the animation
                        } else {
                            instance[i].currentFrame += 1;
                        }
                    }
                    if (animationList[instance[i].currentAnimation].type === 'stopped') {
                        //don't do anything, just let the user decide how to handle the animation
                    }
                    instance[i].animationTimer = 0;
                }
            }
            context.globalAlpha = 1;
            context.restore();
        }

        this.createInstance = function (x, y, initialAnimation) {
            instance.push({
                x: x,
                y: y,
                width: originalWidth,
                height: originalHeight,
                angle: 0,
                alpha: 1,
                currentAnimation: initialAnimation,
                currentFrame: 0,
                animationTimer: 0
            });
            count++;
        }

        this.removeInstance = function (i) {
            instance.splice(i, 1);
            count--;
        }

        this.removeAll = function () {
            instance = [];
            count = 0;
        }

        this.setPosition = function (i, x, y) {
            instance[i].x = x;
            instance[i].y = y;
        }

        this.setAngle = function (i, angle) {
            instance[i].angle = angle;
        }

        this.setAlpha = function (i, alpha) {
            if (alpha >= 1) {
                instance[i].alpha = 1;
            } else if (alpha < 0) {
                instance[i].alpha = 0;
            } else {
                instance[i].alpha = alpha;
            }
        }

        this.setDimensions = function (i, width, height) {
            instance[i].width = width;
            instance[i].height = height;
        }

        this.setPivot = function (i, width, height) {
            pivot = {
                x: x,
                y: y
            };
        }

        this.setCurrentAnimation = function (i, animationName) {
            instance[i].currentAnimation = animationName;
            instance[i].animationTimer = 0;
        }

        this.setCurrentFrame = function (i, newCurrentFrame) {
            instance[i].currentFrame = newCurrentFrame;
        }

        this.getOriginalDimensions = function () {
            return {
                originalWidth: originalWidth,
                originalHeight: originalHeight
            };
        }

        this.getInstanceData = function (i) {
            return instance[i];
        }

        this.getPivot = function () {
            return pivot;
        }

        this.getCount = function () {
            return count;
        }

        this.getType = function () {
            return type;
        }

        this.isDrawable = function () {
            return drawable;
        }

    }

    function StaticSprite(sprite) {
        var type = sprite.type;
        var image = sprite.image;
        var originalWidth = sprite.width;
        var originalHeight = sprite.height;
        var drawable = true;
        var instance = [];
        if (sprite.pivot !== undefined) {
            var pivot = {
                x: sprite.pivot.x,
                y: sprite.pivot.y
            }
        } else {
            var pivot = {
                x: 0,
                y: 0
            }
        }
        var count = 0;
        /*
        the actual draw function that draws on the game context
        */
        this.draw = function (context) {
            for (var i = 0; i < count; i++) {
                context.save();
                context.translate(instance[i].x, instance[i].y);
                context.rotate(instance[i].angle * (Math.PI / 100));
                context.globalAlpha = instance[i].alpha;
                context.drawImage(image, parseInt(-instance[i].width * pivot.x), parseInt(-instance[i].height * pivot.y), instance[i].width, instance[i].height);
                context.globalAlpha = 1;
                context.restore();
            }
        }

        this.createInstance = function (x, y) {
            instance.push({
                x: x,
                y: y,
                angle: 0,
                alpha: 1,
                width: originalWidth,
                height: originalHeight
            });
            count++;
        }

        this.removeInstance = function (i) {
            instance.splice(i, 1);
            count--;
        }

        this.removeAll = function () {
            instance = [];
            count = 0;
        }

        this.setPosition = function (i, x, y) {
            instance[i].x = x;
            instance[i].y = y;
        }

        this.setAngle = function (i, angle) {
            instance[i].angle = angle;
        }

        this.setAlpha = function (i, alpha) {
            if (alpha >= 1) {
                instance[i].alpha = 1;
            } else if (alpha < 0) {
                instance[i].alpha = 0;
            } else {
                instance[i].alpha = alpha;
            }
        }

        this.setDimensions = function (i, width, height) {
            instance[i].width = width;
            instance[i].height = height;
        }

        this.setPivot = function (i, width, height) {
            pivot = {
                x: x,
                y: y
            };
        }

        this.getImage = function () {
            return image;
        }

        this.getOriginalDimensions = function () {
            return {
                originalWidth: originalWidth,
                originalHeight: originalHeight
            };
        }

        this.getInstanceData = function (i) {
            return instance[i];
        }

        this.getPivot = function () {
            return pivot;
        }

        this.getCount = function () {
            return count;
        }

        this.getType = function () {
            return type;
        }

        this.isDrawable = function () {
            return drawable;
        }
    }

    var Shapes = {
        /*
        Draws a rectangle, a rectangle object needs to be passed
        rectangle = {
            type: "fill"/"stroke"/"fillStroke",
            x: 100,
            y: 100,
            height: 100,
            width: 100,
            for "fill"
                color: "#FFFFFF"
            for "stroke"
                strokeColor: "#FFFFFF",
                thickness: 10
            for "fillStroke"
                color: "#FFFFFF",
                strokeColor: "#FFFFFF",
                thickness: 10
        }
        */
        rectangle: function (scene, rectangle) {
            if (rectangle.type === "fill") {

                function drawRectangle(scene, rectangle) {
                    this.draw = function () {
                        if (rectangle.color !== undefined)
                            scene.getGame().getContext().fillStyle = rectangle.color;
                        scene.getGame().getContext().fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
                    }
                }
                scene.drawStack.push(new drawRectangle(scene, rectangle));
            }

            if (rectangle.type === "stroke") {
                function drawRectangle(scene, rectangle) {
                    this.draw = function () {
                        if (rectangle.strokeColor !== undefined)
                            scene.getGame().getContext().strokeStyle = rectangle.strokeColor;
                        if (rectangle.thickness !== undefined)
                            scene.getGame().getContext().lineWidth = rectangle.thickness;
                        scene.getGame().getContext().strokeRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
                    }
                }
                scene.drawStack.push(new drawRectangle(scene, rectangle));
            }

            if (rectangle.type === "fillStroke") {
                function drawRectangle(scene, rectangle) {
                    this.draw = function () {
                        if (rectangle.color !== undefined)
                            scene.getGame().getContext().fillStyle = rectangle.color;
                        if (rectangle.strokeColor !== undefined)
                            scene.getGame().getContext().strokeStyle = rectangle.strokeColor;
                        if (rectangle.thickness !== undefined)
                            scene.getGame().getContext().lineWidth = rectangle.thickness;
                        scene.getGame().getContext().fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
                        scene.getGame().getContext().strokeRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
                    }
                }
                scene.drawStack.push(new drawRectangle(scene, rectangle));
            }
        },


        /*
            Draws a circle
            circle = {
                x: 100,
                y: 100,
                radius: 10,
                type: "stroke", "fill", "fillStroke"
            }
        */
        circle: function (scene, circle) {
            var context = scene.getGame().getContext();
            if (circle.type === "fill") {
                function drawCircle(scene, context, circle) {
                    this.draw = function () {
                        if (circle.color !== undefined)
                            context.fillStyle = circle.color;
                        context.beginPath();
                        context.arc(circle.x, circle.y, circle.radius, 0, Math.PI * 2, false);
                        context.fill();
                    }
                }
            }

            if (circle.type === "stroke") {
                function drawCircle(scene, context, cirlce) {
                    this.draw = function () {
                        if (circle.strokeColor !== undefined)
                            context.strokeStyle = cirlce.strokeColor;
                        if (circle.thickness !== undefined)
                            context.lineWidth = circle.thickness;
                        context.beginPath();
                        context.arc(circle.x, circle.y, circle.radius, 0, Math.PI * 2, false);
                        context.stroke();
                    }
                }
            }

            if (circle.type === "fillStroke") {
                function drawCircle(scene, context, cirlce) {
                    this.draw = function () {
                        if (circle.color !== undefined)
                            context.fillStyle = circle.color;
                        if (circle.strokeColor !== undefined)
                            context.strokeStyle = cirlce.strokeColor;
                        if (circle.thickness !== undefined)
                            context.lineWidth = circle.thickness;
                        context.beginPath();
                        context.arc(circle.x, circle.y, circle.radius, 0, Math.PI * 2, false);
                        context.fill();
                        context.stroke();
                    }
                }
            }
            scene.drawStack.push(new drawCircle(scene, context, circle));
        },

        line: function (scene, line) {
            var context = scene.getGame().getContext();

            function drawLine(scene, context, line) {
                this.draw = function () {
                    if (line.strokeColor !== undefined)
                        context.strokeStyle = line.strokeColor;
                    if (line.thickness !== undefined)
                        context.lineWidth = line.thickness;
                    context.beginPath();
                    context.moveTo(line.startX, line.startY);
                    context.lineTo(line.endX, line.endY);
                    context.stroke();
                }
            }
            scene.drawStack.push(new drawLine(scene, context, line));
        },
        text: function(scene, text) {
            var context = scene.getGame().getContext();
            function drawText(scene, context, text) {
                this.draw = function() {
                    if(text.font !== undefined && text.fontSize !== undefined) 
                        context.font = (text.fontSize+" "+text.font);
                    if(text.color !== undefined) 
                        context.fillStyle = text.color;
                    context.fillText(text.content, text.x, text.y);
                }
            }
            scene.drawStack.push(new drawText(scene, context, text));
        }
    }

    var Utils = {
        clearScreen: function (scene, color) {
            if (color !== undefined) {
                function drawClearScreen(scene, color) {
                    this.draw = function () {
                        var game = scene.getGame();
                        scene.getGame().getContext().fillStyle = color;
                        game.getContext().fillRect(0, 0, game.getDimensions().width, game.getDimensions().height);
                    }
                }

                scene.drawStack.splice(0, 0, new drawClearScreen(scene, color));
            }
        },
        rectCollide: function (x1, y1, w1, h1, x2, y2, w2, h2) {
            if ((x1 < (x2 + w2)) && ((x1 + w1) > x2) && (y1 < (y2 + h2)) && ((y1 + h1) > y2))
                return true;
            else
                return false;
        },
        circleCollide: function (x1, y1, r1, s2, y2, r2) {
            var distance = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
            if (distance < (r1 + r2))
                return true;
            else
                return false;
        }
    }

    function Kb(g) {
        var game = g;
        var keys = new Array(256);
        this.isKeyDown = function (keyCode) {
            if (keys[keyCode])
                return 1;
            else
                return 0;
        }
        var isKeyDownCallback = function (e) {
            keys[e.keyCode] = 1;
        }
        var isKeyUpCallback = function (e) {
            keys[e.keyCode] = 0;
        }
        game.getCanvas().addEventListener('keydown', isKeyDownCallback.bind(this), false);
        game.getCanvas().addEventListener('keyup', isKeyUpCallback.bind(this), false);
    }

    function Mouse(g) {
        /*
        0 - not pressed
        1 - button pressed
        2 - button down
        3 - button released
        */
        var game = g;
        var mouseX = 0;
        var mouseY = 0;
        var button = [0, 0, 0, 0]
        var getMousePositionCallback = function (e) {
            var rect = game.getCanvas().getBoundingClientRect();
            mouseX = e.clientX - rect.left;
            mouseY = e.clientY - rect.top;
        }

        var isButtonDownCallBack = function (e) {
            //console.log("called");
            if(button[e.which] === 0) {        
                button[e.which] = 1;
            } else if(button[e.which] === 1) {
                button[e.which] = 2;
            }
        }

        var isButtonUpCallBack = function (e) {
            if(button[e.which] === 1 || button[e.which] === 2)
                button[e.which] = 3;
            else if(button[e.which] === 3)
                button[e.which] = 0;
        }

        this.isButtonDown = function (btn) {
            if (button[btn] === 2)
                return 1;
            else
                return 0;
        }
        
        this.isButtonPressed = function(btn) {
            if(button[btn] === 1) {
                isButtonDownCallBack({which:btn});
                return 1;
            }
            else 
                return 0;
            
        }
        
        this.isButtonReleased = function(btn) {
            if(button[btn] === 3) {
                isButtonUpCallBack({which:btn});
                return 1;
            }
            else
                return 0;
        }
        
        this.getMousePos = function () {
            return {
                x: mouseX,
                y: mouseY
            }
        }

        this.getButton = function() {
            return button;
        }
        game.getCanvas().addEventListener('mousemove', getMousePositionCallback.bind(this), false);
        game.getCanvas().addEventListener('mousedown', isButtonDownCallBack.bind(this), false);
        game.getCanvas().addEventListener('mouseup', isButtonUpCallBack.bind(this), false);
    }
    return {
        Game: Game,
        Scene: Scene,
        //StaticSprite: StaticSprite,
        Shapes: Shapes,
        Utils: Utils,
        Kb: Kb,
        Mouse: Mouse
    }
})();


