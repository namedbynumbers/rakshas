var g = new rakshas.Game("gameDiv", "g", 320, 240);
var m = new rakshas.Mouse(g);
var k = new rakshas.Kb(g);



g.load({}, function () {
    var scene = new rakshas.Scene(g, {
        entities: [],
        localData: { x:0, y:0 },
        init: function () {
            g.setPixelatedRendering(true);
            
        },
        update: function () {
            var LD = this.localData;
            rakshas.Utils.clearScreen(this, "#FFFFFF");
            
            var color = "#FF0000";
            if(k.isKeyDown(37)) 
                LD.x -= 5;
            if(k.isKeyDown(38)) 
                LD.y -= 5;
            if(k.isKeyDown(39)) 
                LD.x += 5;
            if(k.isKeyDown(40)) 
                LD.y += 5;    
            rakshas.Shapes.rectangle(this, {
                type: "fill",
                color: "#0000FF",
                x: LD.x,
                y: LD.y,
                width: 20,
                height: 20
            });
            
            rakshas.Shapes.rectangle(this, {
                type: "fill",
                color: color,
                x: m.getMousePos().x,
                y: m.getMousePos().y,
                width: 50,
                height: 50
            });
        }
    });
    g.setActiveScene(scene);
    g.start();
});