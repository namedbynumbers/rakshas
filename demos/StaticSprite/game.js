var g = new rakshas.Game("gameDiv", "g", 320, 240);
var m = new rakshas.Mouse(g);
var k = new rakshas.Kb(g);

var imagelist = {
    player: "the_man.png",
}

g.load(imagelist, function () {
    player = {
            name: "player",
            type: "StaticSprite",
            image: g.getImages().player,
            width: 16,
            height: 58
    };
    var scene = new rakshas.Scene(g, {
        entities: [player],
        localData: {},
        init: function () {
            g.setPixelatedRendering(true);
            this.entities.player.createInstance(160, 120);
        },
        update: function () {
            rakshas.Utils.clearScreen(this, "#FFFFFF");
        }
    });
    g.setActiveScene(scene);
    g.start();
});