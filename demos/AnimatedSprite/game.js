var g = new rakshas.Game("gameDiv", "g", 320, 240);
var m = new rakshas.Mouse(g);
var k = new rakshas.Kb(g);

var imagelist = {
    ball0: "ball/0.png",
    ball1: "ball/1.png",
    ball2: "ball/2.png",
    ball3: "ball/3.png",
    ball4: "ball/4.png",
    ball5: "ball/5.png",
}

g.load(imagelist, function () {

    
    var ball = {
        name: "ball",
        type: "AnimatedSprite",
        animationList: {
            rotate : {
                type: 'repeat', //can be 'repeat', 'once', 'stopped'
                speed: 0.3,
                frames : [g.getImages().ball0,g.getImages().ball1,g.getImages().ball2,g.getImages().ball3,g.getImages().ball4,g.getImages().ball5]
            }
        },
        width: 32,
        height: 32
    }
    var scene = new rakshas.Scene(g, {
        entities: [ball],
        localData: {},
        init: function () {
            g.setPixelatedRendering(true);
            this.entities.ball.createInstance(160, 120, "rotate"); //"rotate" is the initial animation
        },
        update: function () {
            rakshas.Utils.clearScreen(this, "#FFFFFF");
        }
    });
    g.setActiveScene(scene);
    g.start();
});