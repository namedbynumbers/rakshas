var g = new rakshas.Game("gameDiv", "g", 320, 240);
var m = new rakshas.Mouse(g);
var k = new rakshas.Kb(g);



g.load({}, function () {
    var scene = new rakshas.Scene(g, {
        entities: [],
        localData: {},
        init: function () {
            g.setPixelatedRendering(true);
            
        },
        update: function () {
            rakshas.Utils.clearScreen(this, "#FFFFFF");
            var color = "#FF0000";
            
            if(rakshas.Utils.rectCollide(270,200,20,20,m.getMousePos().x,m.getMousePos().y,50,50)) 
                color = "#00FF00";
                
            rakshas.Shapes.rectangle(this, {
                type: "fill",
                color: "#0000FF",
                x: 270,
                y: 200,
                width: 20,
                height: 20
            });
            
            rakshas.Shapes.rectangle(this, {
                type: "fill",
                color: color,
                x: m.getMousePos().x,
                y: m.getMousePos().y,
                width: 50,
                height: 50
            });
        }
    });
    g.setActiveScene(scene);
    g.start();
});