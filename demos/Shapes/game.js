var g = new rakshas.Game("gameDiv", "g", 320, 240);
var m = new rakshas.Mouse(g);
var k = new rakshas.Kb(g);



g.load({}, function () {
    var scene = new rakshas.Scene(g, {
        entities: [],
        localData: {},
        init: function () {
            g.setPixelatedRendering(true);
            
        },
        update: function () {
            rakshas.Utils.clearScreen(this, "#FFFFFF");
            rakshas.Shapes.rectangle(this, {
                type: "fill",
                color: "#FF0000",
                x: 160,
                y: 120,
                width: 50,
                height: 50
            });
             rakshas.Shapes.circle(this, {
                type: "fill",
                color: "#00FF00",
                x: 100,
                y: 100,
                radius: 10
            });

            rakshas.Shapes.line(this, {
                strokeColor: "#FF0000",
                thickness: 2,
                startX: 100,
                startY: 100,
                endX: 200,
                endY: 200
            });
        }
    });
    g.setActiveScene(scene);
    g.start();
});