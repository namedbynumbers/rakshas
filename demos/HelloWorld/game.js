var g = new rakshas.Game("gameDiv", "g", 320, 240);
var m = new rakshas.Mouse(g);
var k = new rakshas.Kb(g);

g.load({}, function () {
    var scene = new rakshas.Scene(g, {
        entities: [],
        localData: {},
        init: function () {
            g.setPixelatedRendering(true);
            
        },
        update: function () {
            rakshas.Utils.clearScreen(this, "#FFFFFF");
            rakshas.Shapes.text(this, {
                content: "Hello, World!",
                color: "red",
                x: 110,
                y:120,
                fontSize:"16px",
                font: "monospace"
            });
        }
    });
    g.setActiveScene(scene);
    g.start();
});